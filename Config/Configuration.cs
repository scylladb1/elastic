﻿using Cassandra;
using Cassandra.Mapping;
using Elasticsearch.Net;
using Microsoft.Extensions.Configuration;
using Nest;
using Nest.JsonNetSerializer;
using Newtonsoft.Json;
using ScyllaElastic.Models;


namespace ScyllaElastic.Config;

public class Configuration
{
    private readonly IConfiguration _configuration;
    public static string LocalConnection { get; set; }
    public static string ELKbaseurl { get; set; }
    public static string ELKConnectionUserName { get; set; }
    public static string ELKSecretKey { get; set; }
    public static ConnectionSettings settings { get; set; }
    public static ElasticClient clientES { get; set; }

    
    public Cluster cluster; // cluster object
    public static Cassandra.ISession session; // session 
    public static IMapper mapper; // mapper for model
    

    public Configuration(IConfiguration configuration) {
        _configuration = configuration;
        
        LocalConnection  = @"Data Source=localhost\sqlexpress; Initial Catalog=db; User Id=sa;Password=Admin@123;Encrypt=False;Integrated Security=False";
        
        ELKbaseurl = "http://localhost:9200";
        
        var pool = new SingleNodeConnectionPool(new Uri(ELKbaseurl));

        settings = new ConnectionSettings(pool,sourceSerializer: (builtin, settings) => new JsonNetSerializer(
                                            builtin, settings,
                                            () => new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore })
                                        ).DefaultFieldNameInferrer(s=>s);
        clientES = new ElasticClient(settings);

        // connect to scylla
        cluster = Cluster.Builder()
            // hosts
            .AddContactPoint("192.168.10.129")
            // username & password
            .WithCredentials("cassandra", "cassandra")
            .Build();
        // connect to stud keyspace
        session = cluster.Connect("stud");

        // register UDT model
        session.UserDefinedTypes.Define(
            //  udt models
            UdtMap.For<kam_udt>(),
            UdtMap.For<vendor_para_udt>(),
            UdtMap.For<party_udt>(),
            UdtMap.For<stone_revise_udt>(),
            UdtMap.For<stone_hit_udt>(),
            UdtMap.For<shipping_udt>(),
            UdtMap.For<rate_amt_percentage_diff_udt>(),
            UdtMap.For<business_logic_udt>(),
            UdtMap.For<remark_udt>(),
            UdtMap.For<para_udt>(),
            UdtMap.For<lot_udt>(),
            UdtMap.For<certificate_number_udt>(),
            UdtMap.For<extra_para_udt>(),
            UdtMap.For<lab_udt>(),
            UdtMap.For<comment_udt>(),
            UdtMap.For<twin_udt>(),
            UdtMap.For<size_udt>(),
            UdtMap.For<layout_udt>(),
            UdtMap.For<order_udt>(),
            UdtMap.For<svs_udt>(),
            UdtMap.For<presentation_udt>(),
            UdtMap.For<inclusion_udt>()
        );
        //MappingConfiguration.Global.Define<ScyllaMapper>();
        // mapper object create
        mapper = new Mapper(session);

    }
    
}