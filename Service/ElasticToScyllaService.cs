﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Nest;
using System.Data;

using ScyllaElastic.Config;
using ScyllaElastic.Models;
using Newtonsoft.Json;


namespace ScyllaElastic.Service;

public class ElasticToScyllaService: BackgroundService
{
    private Configuration config;
    private readonly IConfiguration _elasticClient;
    public ElasticToScyllaService(IConfiguration elasticClient)
    {
        config = new Configuration(elasticClient);
        _elasticClient = elasticClient;
        
    }

    public async Task  AddInScylla() 
    {
        var scroll = new Time(15,TimeUnit.Second);
        var res = await Configuration.clientES.SearchAsync<ElasticModel>(
            i => i.Index(Config.Indices.get_data)
            .Query(q=>q.MatchAll())
            .Size(10000).Scroll(scroll)
            );
        var result = new List<ElasticModel>();
        var list_ids = new List<string>();
        try
            {
                do
                { 
                    if (!res.IsValid || string.IsNullOrEmpty(res.ScrollId))
                        throw new Exception("Search Error");
                    if (!res.Documents.Any())
                        break;
                    // get _source list
                    result.AddRange(res.Documents);
                    // get _id list
                    list_ids.AddRange(res.Hits.Select(s=>s.Id));
                    res = await Configuration.clientES.ScrollAsync<ElasticModel>(scroll, res.ScrollId);
                    //Console.WriteLine(result.Count + " get from " + res.Total);
                
                } while (res.Total >= 0 && res.Total >= result.Count && res.Documents.Any() && res.IsValid);


                // forloop
                foreach (ElasticModel model in result) 
                {
                    // model name same as table name
                    stone_data smodel=new stone_data();
                    
                    smodel.presentation_data = JsonConvert.DeserializeObject<presentation_udt>(JsonConvert.SerializeObject(model.presentation_data));
                    smodel.prfs_packet_id = model.prfs_packet_id;
                    smodel.purchase_rate = model.purchase_rate;
                    smodel.rapnet_market_rate = model.rapnet_market_rate;
                    smodel.rate_amt_percentage_diff = JsonConvert.DeserializeObject<rate_amt_percentage_diff_udt>(JsonConvert.SerializeObject(model.rate_amt_percentage_diff));
                    smodel.rate_freeze = model.rate_freeze;
                    smodel.sale_transfer_datetime = model.sale_transfer_datetime;
                    smodel.shape_data = JsonConvert.DeserializeObject<para_udt>(JsonConvert.SerializeObject(model.shape_data));
                    smodel.shape_description = model.shape_description;
                    smodel.shipping_data = JsonConvert.DeserializeObject<shipping_udt>(JsonConvert.SerializeObject(model.shipping_data));
                    smodel.size_data = JsonConvert.DeserializeObject<size_udt>(JsonConvert.SerializeObject(model.size_data));
                    smodel.skip_age = (int) model.skip_age;
                    smodel.srk_certificate_clarity_data = JsonConvert.DeserializeObject<para_udt>(JsonConvert.SerializeObject(model.srk_certificate_clarity_data));
                    smodel.srk_certificate_color_data = JsonConvert.DeserializeObject<para_udt>(JsonConvert.SerializeObject(model.srk_certificate_color_data));
                    smodel.srk_certificate_cut_data = JsonConvert.DeserializeObject<para_udt>(JsonConvert.SerializeObject(model.srk_certificate_cut_data));
                    smodel.srk_certificate_flo_data = JsonConvert.DeserializeObject<para_udt>(JsonConvert.SerializeObject(model.srk_certificate_flo_data));
                    smodel.srk_certificate_polish_data = JsonConvert.DeserializeObject<para_udt>(JsonConvert.SerializeObject(model.srk_certificate_polish_data));
                    smodel.srk_certificate_sym_data = JsonConvert.DeserializeObject<para_udt>(JsonConvert.SerializeObject(model.srk_certificate_sym_data));
                    smodel.star_length = model.star_length;
                    smodel.stock_vs_sale_data = JsonConvert.DeserializeObject<svs_udt>(JsonConvert.SerializeObject(model.stock_vs_sale_data));
                    smodel.stone_confirm_datetime = model.stone_confirm_datetime;
                    smodel.stone_current_age = (int) model.stone_current_age;
                    smodel.stone_hit_data = JsonConvert.DeserializeObject<stone_hit_udt>(JsonConvert.SerializeObject(model.stone_hit_data));
                    smodel.stone_inward_age = (int) model.stone_inward_age;
                    smodel.stone_original_age = (int) model.stone_original_age;
                    smodel.stone_revise_data = JsonConvert.DeserializeObject<stone_revise_udt>(JsonConvert.SerializeObject(model.stone_revise_data));
                    smodel.sym_data = JsonConvert.DeserializeObject<para_udt>(JsonConvert.SerializeObject(model.sym_data));
                    smodel.system_packet_percentage = model.system_packet_percentage;
                    smodel.tabled = model.tabled;
                    smodel.term_percentage = model.term_percentage;
                    smodel.tinge_col_code = model.tinge_col_code;
                    smodel.total_depth = model.total_depth;
                    smodel.transaction_date = Convert.ToDateTime(model.transaction_date);
                    smodel.transaction_id = model.transaction_id;
                    smodel.twin_data = JsonConvert.DeserializeObject<twin_udt>(JsonConvert.SerializeObject(model.twin_data));
                    smodel.user_code = model.user_code;
                    smodel.vendor_clarity_data = JsonConvert.DeserializeObject<vendor_para_udt>(JsonConvert.SerializeObject(model.vendor_clarity_data));
                    smodel.vendor_color_data = JsonConvert.DeserializeObject<vendor_para_udt>(JsonConvert.SerializeObject(model.vendor_color_data));
                    smodel.vendor_cut_data = JsonConvert.DeserializeObject<vendor_para_udt>(JsonConvert.SerializeObject(model.vendor_cut_data));
                    smodel.vendor_floro_data = JsonConvert.DeserializeObject<vendor_para_udt>(JsonConvert.SerializeObject(model.vendor_floro_data));
                    smodel.vendor_issue_carat = model.vendor_issue_carat;
                    smodel.vendor_polish_data = JsonConvert.DeserializeObject<vendor_para_udt>(JsonConvert.SerializeObject(model.vendor_polish_data));
                    smodel.vendor_stone_inward_age = (int) model.vendor_stone_inward_age;
                    smodel.vendor_sym_data = JsonConvert.DeserializeObject<vendor_para_udt>(JsonConvert.SerializeObject(model.vendor_sym_data));
                    smodel.weekly_volume_percentage = model.weekly_volume_percentage;
                    smodel.certificate_info_data = JsonConvert.DeserializeObject<List<certificate_number_udt>>(JsonConvert.SerializeObject(model.certificate_info_data));
                    smodel.floro_remark_data = JsonConvert.DeserializeObject<List<remark_udt>>(JsonConvert.SerializeObject(model.floro_remark_data));
                    smodel.grading_remark_data = JsonConvert.DeserializeObject<List<remark_udt>>(JsonConvert.SerializeObject(model.grading_remark_data));
                    //smodel.inclusion_data = JsonConvert.DeserializeObject<List<inclusion_udt>>(JsonConvert.SerializeObject(model.inclusion_data));
                    smodel.key_to_symbol_data = JsonConvert.DeserializeObject<List<comment_udt>>(JsonConvert.SerializeObject(model.key_to_symbol_data));
                    smodel.lab_comment_data = JsonConvert.DeserializeObject<List<comment_udt>>(JsonConvert.SerializeObject(model.lab_comment_data));
                    smodel.mat_remark_data = JsonConvert.DeserializeObject<List<remark_udt>>(JsonConvert.SerializeObject(model.mat_remark_data));
                    smodel.mfg_comment_data = JsonConvert.DeserializeObject<List<comment_udt>>(JsonConvert.SerializeObject(model.mfg_comment_data));
                    smodel.mfg_remark_data = JsonConvert.DeserializeObject<List<remark_udt>>(JsonConvert.SerializeObject(model.mfg_remark_data));
                    smodel.polish_remark_data = JsonConvert.DeserializeObject<List<remark_udt>>(JsonConvert.SerializeObject(model.polish_remark_data));
                    smodel.sgs_comment_data = JsonConvert.DeserializeObject<List<comment_udt>>(JsonConvert.SerializeObject(model.sgs_comment_data));
                    smodel.symmentry_remark_data = JsonConvert.DeserializeObject<List<remark_udt>>(JsonConvert.SerializeObject(model.symmentry_remark_data));
                    //smodel.vendor_inclusion_data = JsonConvert.DeserializeObject<List<inclusion_udt>>(JsonConvert.SerializeObject(model.vendor_inclusion_data));

                    // insert into scylla
                   Configuration.mapper.Insert<stone_data>(smodel);
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally {
                var clearScrollResponse = Configuration.clientES.ClearScrollAsync(c=>c.ScrollId(res.ScrollId));
            }


    }

    public override Task StartAsync(CancellationToken cancellationToken)
    {
        return base.StartAsync(cancellationToken);
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        await AddInScylla();
    }

}
